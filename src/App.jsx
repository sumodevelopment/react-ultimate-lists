import React, { lazy, Suspense } from 'react'
import './App.css'
import Navbar from './Navbar/Navbar';
import {
    BrowserRouter,
    Switch,
    Route,
    Redirect
} from 'react-router-dom'
import { AppProvider } from './Context/AppContext';

const TodoList = lazy(() => import('./TodoList/TodoList'));
const PlaceList = lazy(() => import('./PlaceList/PlaceList'));

function App() {
    return (
        <BrowserRouter>
            <AppProvider>
                <div className="App">
                    <Navbar />
                    <Suspense fallback={<div>Loading...</div>}>
                        <Switch>
                            <Route path="/todos" component={TodoList} />
                            <Route path="/places" component={PlaceList} />
                            <Route exact path="/">
                                <Redirect to="/todos" />
                            </Route>
                        </Switch>
                    </Suspense>
                </div>
            </AppProvider>
        </BrowserRouter>
    );
}

export default App;
