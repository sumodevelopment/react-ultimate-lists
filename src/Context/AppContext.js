import React, { createContext, useState } from 'react'

export const AppContext = createContext({
    todos: [],
    places: []
})

export function AppProvider(props) {

    const [ todos, setTodos ] = useState( [] )
    const [ places, setPlaces ] = useState( [] )
    
    const AppState = {
        todos,
        setTodos,
        places,
        setPlaces
    }

    return (
        <AppContext.Provider value={ AppState }>
            { props.children }
        </AppContext.Provider>
    )
}