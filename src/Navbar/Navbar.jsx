import styles from './Navbar.module.css'
import { NavLink } from 'react-router-dom'
import { useContext } from 'react'
import { AppContext } from '../Context/AppContext'

function Navbar() {

    const { todos } = useContext( AppContext )

    return (
        <nav className={styles.Navbar}>
            <ul className={styles.NavbarItems}>
                <li className={ styles.NavbarItem }><i className="material-icons">playlist_add</i></li>
                <li className={ styles.NavbarItem }>
                    <NavLink to="/todos">Todos</NavLink>
                </li>
                <li className={ styles.NavbarItem }>
                    <NavLink to="/places">Places</NavLink>
                </li>
            </ul>
            <ul className={`${styles.NavbarItems} ${styles.NavbarItemsRight}`}>
                <li>Items: { todos.length }</li>
            </ul>
        </nav>
    )
}

export default Navbar