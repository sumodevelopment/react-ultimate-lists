import React, { useState } from 'react'
import Searchbar from '../Searchbar/Searchbar'
import PlaceListItem from './PlaceListItem'
import { filterPlaces, MOCK_PLACES } from './PlacesAPI'

function PlaceList() {

    const [places, setPlaces] = useState(MOCK_PLACES)
    const [searchText, setSearchText] = useState('')

    const handleSearchChange = (event) => {
        setSearchText(event.target.value.trim())
    }

    return (
        <main className="container">
            <h1>My Places</h1>
            <Searchbar
                placeholderText="Search for places"
                onSearchChanged={handleSearchChange}
            />
            <ul>
                {places.filter(place => filterPlaces(place, searchText)).map(place => (
                    <PlaceListItem place={place} key={place.id} />
                ))}
            </ul>
        </main>

    )
}

export default PlaceList