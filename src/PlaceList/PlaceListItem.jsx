import React from 'react'

function PlaceListItem({ place }) {
    return (
        <li>
            <h4>{ place.name }</h4>
            <p>{ place.address }</p>
        </li>
    )
}

export default PlaceListItem