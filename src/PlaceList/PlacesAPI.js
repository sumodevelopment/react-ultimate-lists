export const MOCK_PLACES = [{
    id: 1,
    name: 'Charlies Pizza',
    address: 'Marine Drive, Port Elizabeth'
}, {
    id: 2,
    name: 'Moms house 🏡',
    address: 'Rivier Straat, Despatch'
}, {
    id: 3,
    name: 'IKEA Furuset',
    address: 'Furuset Skole, Oslo'
}]

export function filterPlaces(place, filter) {
    return place.name.toLowerCase().includes(filter.toLowerCase()) ||
           place.address.toLowerCase().includes(filter.toLowerCase())
}