import React from 'react'
import styles from './Searchbar.module.css'

function Searchbar({ placeholderText, onSearchChanged }) {

    return (
        <div className={ styles.SearchContainer }>
            <input className={ styles.SearchInput } type="text" placeholder={ placeholderText } onChange={ onSearchChanged }/>
        </div>
    )
}

export default Searchbar