export const MOCK_TODOS = [{
    id: 1,
    title: 'Learn React Basics',
    completed: false
}, {
    id: 2,
    title: 'Learn React ContextAPI'
}, {
    id: 3,
    title: 'Master React... Eventually'
}]

export function filterTodo(todo, filter) {
    return todo.title.toLowerCase().includes( filter.toLowerCase() ) 
}