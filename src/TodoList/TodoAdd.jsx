import React, { useContext, useState } from 'react';
import { AppContext } from '../Context/AppContext';

export function TodoAdd() {
    const [ todoTitle, setTodoTitle ] = useState('')
    const { todos, setTodos } = useContext( AppContext )

    function onTodoTitleChange(e) {
        setTodoTitle( e.target.value.trim() )
    }

    function addTodo() {
        if (todoTitle.length === 0) {
            return alert('Please type a title')
        }

        setTodos([...todos, {
            id: Math.random().toString(24).slice(2, 8),
            title: todoTitle,
            completed: false
        }]);
    }

    return (
        <div>
            <input type="text" placeholder="Todo title" onChange={ onTodoTitleChange } />
            <button onClick={ addTodo }>Add</button>
        </div>
    )
}