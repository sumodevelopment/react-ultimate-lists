import React, { useContext, useState } from 'react'
import TodoListItem from './TodoListItem'
import Searchbar from '../Searchbar/Searchbar'
import { filterTodo } from './TodoAPI'
import { useHistory } from 'react-router-dom'
import { TodoAdd } from './TodoAdd'
import { AppContext } from '../Context/AppContext'

function TodoList() {

    const history = useHistory()
    const { todos } = useContext( AppContext )
    const [searchText, setSearchText] = useState('')

    const handleSearchChange = (event) => {
        setSearchText(event.target.value.trim())
    }

    return (

        <main className="container">
            <h1>My Todos</h1>
            <Searchbar
                placeholderText="Search for todos"
                onSearchChanged={handleSearchChange}
            />
            <TodoAdd />
            <ul>
                {todos.filter((todo) => filterTodo(todo, searchText)).map(todo => (
                    <TodoListItem title={todo.title} key={todo.id} />
                ))}
            </ul>
            <button onClick={() => history.push("/places")}>View Places</button>
        </main>

    )
}

export default TodoList